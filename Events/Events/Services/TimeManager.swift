//
//  TimeManager.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import Foundation


final class TimeManager {
    
    // MARK: - Setups
    
    static let shared = TimeManager()
    
    
    
    
    
    
    
    // MARK: - Functions
    
    
    /// the function will convert user friendly time
    /// - Parameter date: date in yyyy-MM-dd'T'HH:mm:ss format
    /// - Returns: string of time in yyyy MMM HH:mm EEEE format
    func convertDateFormatter(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.locale = Locale(identifier: "your_loc_id")
        let convertedDate = dateFormatter.date(from: date)

        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }

        dateFormatter.dateFormat = "yyyy MMM HH:mm EEEE"///this is what you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: convertedDate!)

        return timeStamp
    }
}
