//
//  DisplayData.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import Foundation

/// used in event view controller as display data model
enum DisplayData {
    
    case events(model:Events)
    
}


/// used in detail event view controller to display model

enum DisplayDetail {
    
    case safari(model:Events)
    case webKit(model:Events)
}


