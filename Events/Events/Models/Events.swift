//
//  Events.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import Foundation



struct Events: Codable {
    
    let id: String
    let name: Name
    let start : Start// Start time
    let end : End //End time
    let logo: Logo // urls of logo
    let url: String
    let vanity_url: String
}


struct Name: Codable {
    
    let text: String
}


struct Start: Codable {
    
    let timezone: String
    let utc: String
    let local: String
}


struct End: Codable {
    
    let timezone: String
    let utc: String
    let local: String
}



struct Logo: Codable {
    
    let url: String
}
