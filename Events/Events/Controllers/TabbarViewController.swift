//
//  TabbarViewController.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import UIKit

class TabbarViewController:  UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let vc = EventsViewController()

        vc.title = "Events"
        vc.navigationItem.largeTitleDisplayMode = .always

        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.tintColor = .label
        nav.tabBarItem = UITabBarItem(title: "Events", image: UIImage(systemName: "list.bullet"), tag: 1)
        nav.navigationBar.prefersLargeTitles = true

        setViewControllers([nav], animated: false)
        
        
    }
    
}
