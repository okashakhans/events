//
//  EventDetailViewController.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import UIKit
import SafariServices

class EventDetailViewController: UIViewController {
    
    // MARK: - Setups
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)

        tableView.register(UITableViewCell.self,forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    private var displayData:[DisplayDetail] = []
    
    private let event: Events
    
    // MARK: - Lifecycle
    
    init(event: Events) {
        
        self.event = event
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        view.addSubview(tableView)
        self.title = event.name.text
        
        self.createTableHeader(with: event.logo.url)
        self.updateUI()
        self.title = event.name.text
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = view.bounds
    }
    
    // MARK: - Functions
    
    private func updateUI() {
        
        displayData.removeAll()
        displayData.append(.safari(model: event))
        displayData.append(.webKit(model: event))

        tableView.isHidden = false
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Table views Configuration
    
    private func createTableHeader(with string: String?) {
        
        guard let urlString = string, let url = URL(string: urlString) else {
            return
        }

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: view.width/1.5))

        let imageSize: CGFloat = headerView.height/2
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: imageSize, height: imageSize))
        imageView.center = headerView.center
        imageView.contentMode = .scaleAspectFill
        imageView.sd_setImage(with: url, completed: nil)
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageSize/2
        imageView.backgroundColor = UIColor.green
        
        headerView.addSubview(imageView)
        
        tableView.tableHeaderView = headerView
    }


}


// MARK: - Extensions (Table View)
extension EventDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as UITableViewCell
        
        let result = displayData[indexPath.row]
        switch  result {
        case .safari(_):
            cell.textLabel?.text = "Open Safari"
        case .webKit(_):
            cell.textLabel?.text = "Open Webkit"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Options"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let result = displayData[indexPath.row]
        
        switch result {
        
            case .safari(_):
                
                let vc = SFSafariViewController(url: URL(string: event.vanity_url)!)
                present(vc, animated: true, completion: nil)
                
            case .webKit(_):
                
                let vc = EventDetailWebKitViewController(url: event.vanity_url)
                vc.title = event.name.text
                vc.navigationItem.largeTitleDisplayMode = .never
                navigationController?.pushViewController(vc, animated: true)
        }
    }
}
