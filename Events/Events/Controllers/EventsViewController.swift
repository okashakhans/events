//
//  EventsViewController.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import UIKit
import SDWebImage

class EventsViewController: UIViewController {

    // MARK: - Setups
    
    private var collectionView: UICollectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewCompositionalLayout { sectionIndex, _ -> NSCollectionLayoutSection? in
            return createSectionLayout(section: sectionIndex)
        }
    )
    
    
    private var displayData: [DisplayData] = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        
        fetchEvents()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    
    
    // MARK: - Functions

    /// The function will call API Manager to fetch events.
    /// If successfull append data in data model.
    /// It will configure collection view and reload data.
    private func fetchEvents(){
        
        APIManager.shared.fetchEvents() { result in
            
            switch result {
            
            case .success(let results):
                
                self.displayData.removeAll()
                self.displayData.append(contentsOf: results.compactMap({.events(model: $0)}))
                self.configureCollectionView()
                self.collectionView.reloadData()
                
            case .failure(let error):  print("Error \(error)")
            }
        }
    }
    
    
    
    
    
    // MARK: - Collection views Configuration
    
    private func configureCollectionView() {
        view.addSubview(collectionView)
        
        collectionView.register(UICollectionViewCell.self,forCellWithReuseIdentifier: "cell")
        collectionView.register(EventsCollectionViewCell.self,forCellWithReuseIdentifier: EventsCollectionViewCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .systemBackground
    }
    
    static func createSectionLayout(section: Int) -> NSCollectionLayoutSection {
        
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
        )

        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)

        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .absolute(80)
            ),
            subitem: item,
            count: 1
        )

        let section = NSCollectionLayoutSection(group: group)
        return section
    }
    
}


// MARK: - Extensions (Collection View)

extension EventsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return displayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let result = displayData[indexPath.row]
        
        switch result {
        
        case.events(model: let event):
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EventsCollectionViewCell.identifier,for: indexPath) as? EventsCollectionViewCell else {return UICollectionViewCell()}
            
            cell.configure(with: event)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       

        let result = displayData[indexPath.row]
        
        switch result {
        
            case.events(model: let event):
                
                let vc = EventDetailViewController(event: event)
                vc.title = event.name.text
                vc.navigationItem.largeTitleDisplayMode = .never
                navigationController?.pushViewController(vc, animated: true)
        }
    }
}
