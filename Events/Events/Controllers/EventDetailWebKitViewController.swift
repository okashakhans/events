//
//  EventDetailWebKitViewController.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import UIKit
import WebKit

class EventDetailWebKitViewController: UIViewController {

    // MARK: - Setups
    
    private let webView: WKWebView = {
 
        let webView = WKWebView()
        return webView
    }()
    
    private var url = String()
    
    // MARK: - Lifecycle
    
    init(url: String) {
        
        self.url = url
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        view.addSubview(webView)
        
       //self.title = "WebKit"
        
        webView.load(URLRequest(url:URL(string: self.url)!))
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        webView.frame = view.bounds
    }
    
}
