//
//  EventsCollectionViewCell.swift
//  Events
//
//  Created by Okasha Khan on 28/05/2021.
//

import UIKit

class EventsCollectionViewCell: UICollectionViewCell {
    
    
    static let identifier = "EventsCollectionViewCell"

    // MARK: - Setups
    private let eventImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "photo")
        imageView.contentMode = .scaleAspectFill
    
        return imageView
    }()

    private let eventNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 18, weight: .regular)
        return label
    }()

    private let eventTimeLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 15, weight: .thin)
        return label
    }()

    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .secondarySystemBackground
        contentView.backgroundColor = .secondarySystemBackground
        contentView.addSubview(eventImage)
        contentView.addSubview(eventNameLabel)
        contentView.addSubview(eventTimeLabel)
        contentView.clipsToBounds = true
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.imageSetup()
        self.labelSetup()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        eventNameLabel.text = nil
        eventImage.image = nil
        eventTimeLabel.text = nil
    }

    
    // MARK: - Funcitons
    
    private func imageSetup(){
        
        let imageSize: CGFloat = contentView.height-10
        eventImage.frame = CGRect(x: 5,y: 2, width: imageSize, height: imageSize)
        eventImage.layer.cornerRadius = imageSize/2
        eventImage.layer.masksToBounds = true
    }
    
    private func labelSetup(){
        
        eventNameLabel.frame = CGRect(x: eventImage.right+10,y: 0,width: contentView.width-eventImage.right-15,height: contentView.height/2)
        eventTimeLabel.frame = CGRect(
            x: eventImage.right+10,
            y: contentView.height/2,
            width: contentView.width-eventImage.right-15,
            height: contentView.height/2
        )
    }
    
    
    // MARK: - External Callers
    
    func configure(with eventModel: Events) {

        eventImage.sd_setImage(with: URL(string:eventModel.logo.url, relativeTo: nil))
        eventNameLabel.text = eventModel.name.text
        
        eventTimeLabel.text = TimeManager.shared.convertDateFormatter(date: eventModel.start.local)

   }
    
}

